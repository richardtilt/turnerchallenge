//
//  SearchViewController.h
//  Challenger
//
//  Created by Richard Tilt on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate>

@property   (strong, nonatomic)     IBOutlet    UITableView     *movieTableView;

@property   (strong, nonatomic)     IBOutlet    UISearchBar     *movieSearchBar;

@property   (strong, nonatomic)                 NSMutableArray  *filteredMovieArray;

@end
