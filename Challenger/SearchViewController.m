//
//  SearchViewController.m
//  Challenger
//
//  Created by Richard Tilt on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import "SearchViewController.h"
#import "DetailViewController.h"
#import "MovieData.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

@synthesize movieSearchBar;
@synthesize movieTableView;
@synthesize filteredMovieArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    // -----------------------------------------------------------------------------
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
    
    }
    
    // -----------------------------------------------------------------------------
    
    return self;
    
    // -----------------------------------------------------------------------------
}

- (void)viewDidLoad {
    
    // -----------------------------------------------------------------------------
    
    [super viewDidLoad];
    
    // -----------------------------------------------------------------------------
    
    [[self navigationController] setNavigationBarHidden:NO];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:nil
                                                                            action:nil];
    
    [self setTitle:@"TBS Movie Search"];
    
    // -----------------------------------------------------------------------------
    
    [[self movieTableView] setDataSource:self];
    
    [[self movieTableView] setDelegate:self];
    
    // -----------------------------------------------------------------------------
    
    [self setFilteredMovieArray:[NSMutableArray arrayWithCapacity:[[[MovieData sharedMovieData] allMovieTitles] count]]];
    
    // -----------------------------------------------------------------------------
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString *)scope {
    
    // -----------------------------------------------------------------------------
    
    [self.filteredMovieArray removeAllObjects];
    
    // -----------------------------------------------------------------------------
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
    
    // -----------------------------------------------------------------------------
    
    [self setFilteredMovieArray:[NSMutableArray arrayWithArray:[[[MovieData sharedMovieData] allMovieTitles] filteredArrayUsingPredicate:predicate]]];
    
    // -----------------------------------------------------------------------------
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // -----------------------------------------------------------------------------
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        return [self.filteredMovieArray count];
        
    } else {
        
        return [[[MovieData sharedMovieData] allMovieTitles] count];
    }
    
    // -----------------------------------------------------------------------------
}

#pragma mark - UITableView Delegate Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // -----------------------------------------------------------------------------
    
    static NSString *CellIdentifier = @"Cell";
    
    // -----------------------------------------------------------------------------
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // -----------------------------------------------------------------------------
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // -----------------------------------------------------------------------------
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        cell.textLabel.text = [self.filteredMovieArray objectAtIndex:indexPath.row];
        
    } else {
        
        cell.textLabel.text = [[[MovieData sharedMovieData] allMovieTitles] objectAtIndex:[indexPath row]];
    }
    
    // -----------------------------------------------------------------------------
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    // -----------------------------------------------------------------------------
    
    return cell;
    
    // -----------------------------------------------------------------------------
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // -----------------------------------------------------------------------------
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // -----------------------------------------------------------------------------
    
    NSString *movieName = [[[tableView cellForRowAtIndexPath:indexPath] textLabel] text];
    
    DetailViewController *detailViewController = [[DetailViewController alloc] initWithMovieTitle:movieName];
    
    [[self navigationController] pushViewController:detailViewController animated:YES];
    
    // -----------------------------------------------------------------------------
}

#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    
    // -----------------------------------------------------------------------------
    
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // -----------------------------------------------------------------------------
    
    return YES;
    
    // -----------------------------------------------------------------------------
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    
    // -----------------------------------------------------------------------------
    
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     
    // -----------------------------------------------------------------------------
     
    [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // -----------------------------------------------------------------------------
    
    return YES;
    
    // -----------------------------------------------------------------------------
}

@end
