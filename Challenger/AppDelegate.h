//
//  AppDelegate.h
//  Challenger
//
//  Created by Tilt, Rich - on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import "SplashViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
