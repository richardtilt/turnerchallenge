//
//  MovieData.h
//  Challenger
//
//  Created by Richard Tilt on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

@interface MovieData : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate> {
    
}

+ (MovieData *)sharedMovieData;

- (void)loadMovieDataWithCallback:(void (^) (NSError *error))handler;

- (NSArray *)allMovieTitles;

- (NSDictionary *)movieDetail:(NSString *)titleName;

@end
