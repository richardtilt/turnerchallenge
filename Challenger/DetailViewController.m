//
//  DetailViewController.m
//  Challenger
//
//  Created by Richard Tilt on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import "DetailViewController.h"
#import "MovieData.h"

@interface DetailViewController () {
    
    NSDictionary *movie;
}

- (void)lookUpCoverArt:(NSString *)key;

@end

@implementation DetailViewController

@synthesize movieTitle, movieYear, movieGenre, movieActors, movieDescription, coverArtImageView;

- (id)initWithMovieTitle:(NSString *)title {
    
    // -----------------------------------------------------------------------------
    
    movie = [[MovieData sharedMovieData] movieDetail:title];
    
    return [self initWithNibName:@"DetailViewController" bundle:nil];
    
    // -----------------------------------------------------------------------------
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    // -----------------------------------------------------------------------------
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    // -----------------------------------------------------------------------------
    
    if (self) {
        
    }
    
    // -----------------------------------------------------------------------------
    
    return self;
    
    // -----------------------------------------------------------------------------
}

- (void)viewDidLoad {
    
    // -----------------------------------------------------------------------------
    
    [super viewDidLoad];
    
    // -----------------------------------------------------------------------------
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Custom Title"
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:nil
                                                                            action:nil];
    
    // -----------------------------------------------------------------------------
    
    [self.navigationItem setHidesBackButton:NO animated:YES];
    
    // -----------------------------------------------------------------------------
    
    if (movie) {
        
        // -----------------------------------------------------------------------------
        
        // NSLog(@"Movie Detail:%@", movie);
        
        [[self movieTitle] setText:[movie objectForKey:@"TitleName"]];
        
        NSNumber *year = [movie objectForKey:@"ReleaseYear"];
        
        [[self movieYear] setText:[year stringValue]];
        
        // -------------------------------------------------------------------------
        
        NSArray *genres = [movie objectForKey:@"Genres"];
        
        [[self movieGenre] setText:[[genres valueForKey:@"description"] componentsJoinedByString:@" "]];
    
        // -------------------------------------------------------------------------
        
        NSArray *participantsTemp = [movie objectForKey:@"Participants"];
        
        NSMutableArray *participants = [NSMutableArray array];
        
        for (int i = 0; i < 5; i++) {
            
            if ([participantsTemp count] > i) {
                
                NSDictionary *participant = [participantsTemp objectAtIndex:i];
                
                if ([[participant objectForKey:@"RoleType"] isEqualToString:@"Actor"]) {
                    
                    [participants addObject:[participant objectForKey:@"Name"]];
                }
            }
        }
        
        [[self movieActors] setText:[[participants valueForKey:@"description"] componentsJoinedByString:@" "]];
        
        // -------------------------------------------------------------------------
        
        NSArray *storyLines = [movie objectForKey:@"Storylines"];
        
        NSDictionary *storyLine = [storyLines objectAtIndex:0];
        
        NSString *desc = [storyLine objectForKey:@"Description"];
        
        [[self movieDescription] setText:desc];
        
        // -------------------------------------------------------------------------
        
        NSArray *externalSources = [movie objectForKey:@"ExternalSources"];
        
        for (NSDictionary *source in externalSources) {
            
            NSString *name = [source objectForKey:@"Name"];
            
            if ([name isEqualToString:@"IMDB"]) {
                
                NSString *apiId = [source objectForKey:@"Key"];
                
                [self lookUpCoverArt:apiId];
            }
        }
        
        // -------------------------------------------------------------------------
    }
    
    // -----------------------------------------------------------------------------
}

- (void)didReceiveMemoryWarning {
    
    // -----------------------------------------------------------------------------
    
    [super didReceiveMemoryWarning];
    
    // -----------------------------------------------------------------------------
}

- (void)lookUpCoverArt:(NSString *)key {
    
    // -----------------------------------------------------------------------------
    
    __block UIImage *image = nil;
    
    // -----------------------------------------------------------------------------
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    dispatch_async(queue, ^(void) {
        
        
        // -------------------------------------------------------------------------
        
        if (key) {
            
            // -------------------------------------------------------------------------
            
            NSString *requestString = [NSString stringWithFormat:@"%@%@", @"http://www.omdbapi.com/?i=", key];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestString]];
            
            NSURLResponse *response = nil;
            
            NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
            
            NSError *error = nil;
            
            NSDictionary *coverData = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if (coverData) {
                
                NSString *poster = [coverData objectForKey:@"Poster"];
                
                if (poster) {
                    
                    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:poster]];
                    
                    NSURLResponse *response = nil;
                    
                    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
                    
                    image = [UIImage imageWithData:data];
                }
            }
            
            // -------------------------------------------------------------------------
        }
        
        // -------------------------------------------------------------------------
        

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[self coverArtImageView] setImage:image];
            
        });
        
    });
    
    // -----------------------------------------------------------------------------
    
    
    
    
    // -----------------------------------------------------------------------------
}

@end
