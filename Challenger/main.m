//
//  main.m
//  Challenger
//
//  Created by Tilt, Rich - on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
