//
//  SplashViewController.m
//  Challenger
//
//  Created by Richard Tilt on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import "SplashViewController.h"
#import "SearchViewController.h"
#import "MovieData.h"

@interface SplashViewController ()

- (void)loadSearchView;

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
    }
    
    return self;
}

- (void)viewDidLoad {
    
    // -----------------------------------------------------------------------------
    
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    // -----------------------------------------------------------------------------
    
    [[MovieData sharedMovieData] loadMovieDataWithCallback:^(NSError * error) {
        
        if (error) {
            
            // ---------------------------------------------------------------------
            
            NSLog(@"Error:%@", [error localizedDescription]);
            
            // ---------------------------------------------------------------------
        }
        else {
            
            // ---------------------------------------------------------------------
            
            [self loadSearchView];
            
            // ---------------------------------------------------------------------
        }
    }];
    
    // -----------------------------------------------------------------------------
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (void)loadSearchView {
    
    // -----------------------------------------------------------------------------
    
    SearchViewController *searchViewController = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    
    [[self navigationController] pushViewController:searchViewController animated:YES];
    
    // -----------------------------------------------------------------------------
}

@end
