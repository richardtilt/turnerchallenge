//
//  MovieData.m
//  Challenger
//
//  Created by Richard Tilt on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import "MovieData.h"
#import "Reachability.h"

@interface MovieData () {
    
    NSArray                 *allMovies;
    
    NSMutableDictionary     *allTitles;
    
    NSMutableData           *networkResponseData;
    
    NSInteger               networkStatusCode;
    
    Reachability            *internetReach;
    
    void (^networkDataCallBack) (NSError *error);
}

@end

@implementation MovieData

+ (MovieData *)sharedMovieData {
    
    // -----------------------------------------------------------------------------
    
    static MovieData *sharedMovieData = nil;
    
    static dispatch_once_t onceToken;
    
    // -----------------------------------------------------------------------------
    
    dispatch_once(&onceToken, ^{
     
        sharedMovieData = [[MovieData alloc] init];
    });
    
    // -----------------------------------------------------------------------------
    
    return sharedMovieData;
    
    // -----------------------------------------------------------------------------
}

- (NSDictionary *)movieDetail:(NSString *)titleName {
    
    // -----------------------------------------------------------------------------
    
    NSDictionary *movieDetail = nil;
    
    // -----------------------------------------------------------------------------
    
    for (NSDictionary *movie in allMovies) {
        
        NSString *sourceId = [movie objectForKey:@"TitleNameSortable"];
        
        if ([sourceId isEqualToString:titleName]) {
            
            movieDetail = movie;
            
            break;
        }
    }
    
    // -----------------------------------------------------------------------------
    
    return movieDetail;
    
    // -----------------------------------------------------------------------------
}

- (NSArray *)allMovieTitles {
    
    // -----------------------------------------------------------------------------
    
    return [allTitles allValues];
    
    // -----------------------------------------------------------------------------
}

- (void)loadMovieDataWithCallback:(void (^) (NSError *error))handler {
    
    // -----------------------------------------------------------------------------
    
    // Hold on to the block until we're all done
    networkDataCallBack = handler;
    
    // -----------------------------------------------------------------------------
    
    // Check for Reachability
    
    internetReach = [Reachability reachabilityForInternetConnection];
    
    // -----------------------------------------------------------------------------
    
    if (internetReach.currentReachabilityStatus == NotReachable) {
        
        // ----------------------------------------------------------------------------
        
        NSError *error = [NSError errorWithDomain:@"com.tbs.com" code:0 userInfo:nil];
        
        networkDataCallBack(error);
        
        // ----------------------------------------------------------------------------
    }
    else {
    
        // ----------------------------------------------------------------------------
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        // ----------------------------------------------------------------------------
        
        NSURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@"http://dc.tbs.io/all"]
                                                                   cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20.0f];
        
        // ----------------------------------------------------------------------------
        // Create the connection
        
        NSURLConnection *networkConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        
        [networkConnection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        
        [networkConnection start];
        
        // ----------------------------------------------------------------------------
        // Check Connection and allocate NSData object
        
        if (networkConnection) {
            
            // ------------------------------------------------------------------------
            
            networkStatusCode = 0;
            
            networkResponseData = [[NSMutableData alloc] init];
            
            // ------------------------------------------------------------------------
        }
        else {
            
            // ------------------------------------------------------------------------
            
            NSError *error = [NSError errorWithDomain:@"com.tbs.com" code:0 userInfo:nil];
            
            networkDataCallBack(error);
            
            networkDataCallBack = nil;
            
            // ------------------------------------------------------------------------
        }
    }
    
    // ----------------------------------------------------------------------------
}

#pragma mark - NSURL Connection Delegate Methods

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    // -----------------------------------------------------------------------------
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    networkDataCallBack(error);
    
    networkDataCallBack = nil;
    
    // -----------------------------------------------------------------------------
}

#pragma mark - NSURLConnection Data Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    // ----------------------------------------------------------------------------
    
    [networkResponseData appendData:data];
    
    // ----------------------------------------------------------------------------
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    // ----------------------------------------------------------------------------
    
    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        networkStatusCode = [httpResponse statusCode];
    }
    
    // ----------------------------------------------------------------------------
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // ----------------------------------------------------------------------------
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    // ----------------------------------------------------------------------------
    
    NSError *error;
    
    allMovies = (NSArray *)[NSJSONSerialization JSONObjectWithData:networkResponseData options:kNilOptions error:&error];
    
    // ----------------------------------------------------------------------------
    
    allTitles = [[NSMutableDictionary alloc] init];
    
    for (NSDictionary *movie in allMovies) {
        
        [allTitles setObject:[movie objectForKey:@"TitleNameSortable"] forKey:[movie objectForKey:@"TitleId"]];
    }

    // ----------------------------------------------------------------------------
    
    networkDataCallBack(error);
    
    // ----------------------------------------------------------------------------
    
    networkDataCallBack = nil;
    
    // ----------------------------------------------------------------------------
}

@end