//
//  DetailViewController.h
//  Challenger
//
//  Created by Richard Tilt on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property   (strong, nonatomic)     IBOutlet    UILabel         *movieTitle;

@property   (strong, nonatomic)     IBOutlet    UILabel         *movieYear;

@property   (strong, nonatomic)     IBOutlet    UILabel         *movieGenre;

@property   (strong, nonatomic)     IBOutlet    UITextView      *movieActors;

@property   (strong, nonatomic)     IBOutlet    UITextView      *movieDescription;

@property   (strong, nonatomic)     IBOutlet    UIImageView     *coverArtImageView;

- (id)initWithMovieTitle:(NSString *)title;

@end
