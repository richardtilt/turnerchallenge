//
//  SplashViewController.h
//  Challenger
//
//  Created by Richard Tilt on 10/30/13.
//  Copyright (c) 2013 Q Microsystems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController

@end
